import os
from rdflib import Graph, URIRef

def retrieve_legislation(uri_stub):
    g = Graph()
    g.parse("http://legislation.data.gov.uk/" + uri_stub + "/data.rdf")
    identifier = "http://www.legislation.gov.uk/id/" + uri_stub
    data = {}

    for stmt in g.subject_objects(URIRef("http://purl.org/dc/terms/title")):
        data["short_title"] = str(stmt[1]).strip()
    for stmt in g.subject_objects(URIRef("http://purl.org/dc/terms/description")):
        data["long_title"] = str(stmt[1]).lstrip(os.linesep).strip()
    for stmt in g.subject_objects(URIRef("http://purl.org/dc/terms/created")):
        data["royal_assent"] = str(stmt[1])
    for stmt in g.subject_objects(URIRef("http://purl.org/dc/terms/valid")):
        valid_value = str(stmt[1])
        start, end = [x.split("=")[-1] for x in valid_value.split(";")]
        
        if start is not '':
            data["commencement_date"] = start
        else:
            data["commencement_data"] = None
        if end is not '':
            data["repeal_date"] = end
        else:
            data["repeal_date"] = None
     
    return data
